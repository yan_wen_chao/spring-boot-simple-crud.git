package com.example.demo.entity;

import lombok.Data;

@Data  //这里引用了data 所以不需要 get/set的方法了
public class UserEntity {

    private String id;//id

    private  String name;//姓名

    private String login; //账号

    private String password;// 密码

}
