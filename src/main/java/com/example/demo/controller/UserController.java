package com.example.demo.controller;


import com.example.demo.entity.UserEntity;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


import java.util.List;

@RequestMapping("user")
@RestController
public class UserController {

    //todo    ====================================后台调用---》   每个控制台上面有  自己复制即可

    @Autowired
    private UserService userService;

    /**
     * 查询所有的数据
     * @return
     *
     * http://localhost:1234/user/list
     */
    @RequestMapping("list")
    public List<UserEntity> findall(){
        return userService.findall();
    }




    /** 当使用@RequestMapping URI id 样式映射时，
     * 即 someUrl/{paramId}, id @Pathvariable注解绑定它传过来的值到方法的参数上。
     *
     * 根据ID查询
     * @param id
     * @return
     *
     * http://localhost:1234/user/select/2
     */
    @RequestMapping("/select/{id}")
    public String selectId(@PathVariable int id){
            return userService.getselectId(id).toString();
    }


    /**
     *
     * 根据ID删除
     * @param id
     * @return
     *
     * http://localhost:1234/user/delect/2
     */
    @RequestMapping("/delect/{id}")
    public String delectId(@PathVariable int id){
      int result=userService.delectId(id);
        if (result>=1){
            return "删除成功";
        }else{
            return "删除失败";
        }
    }



    /**
     * http://localhost:1234/user/index
     * 展示页面
     * @return
     */
    @RequestMapping("/index")
    public ModelAndView index(){
        return new ModelAndView("index");

    }





    /**
     * http://localhost:1234/user/ahtml
     * 新增页面
     * @return
     */
    @RequestMapping("/ahtml")
    public ModelAndView adds(){
        return new ModelAndView("add");

    }





    /**
     *  新增数组
     * @param userEntity
     * @return
     *http://localhost:1234/user/adds
     *
     * http://localhost:1234/user/ahtml  用这个
     * 返回值是IdUserEntity(id=11, name=22, login=33, password=44)
     */

    @RequestMapping("/adds")
    public  String  add(UserEntity userEntity){

        String str = "返回值是"+"Id"+userEntity;
        System.out.println(str);

        int result=userService.addall(userEntity);
        if (result>=1){
            return "添加成功";
        }else{
            return "添加失败";
        }


    }

    /**
     *  修改
     * @param userEntity
     * @return
     *
     * http://localhost:1234/user/updatess
     */
        @RequestMapping("/updatess")
        public  String Upate(UserEntity userEntity){
            //这里是需要修改的数据  如果id么有就算新增
            userEntity.setId("2");
            userEntity.setName("不知道");
            userEntity.setLogin("yan");
            userEntity.setPassword("78578578");

          int results=userService.update(userEntity);

            if(results==1){
                return "更新成功";
            }
            return "更新失败";
        }





}
