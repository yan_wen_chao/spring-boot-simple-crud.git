package com.example.demo.controller;

import com.example.demo.entity.UserEntity;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collection;

@Controller
public class HtmlController {

//todo    ====================================页面调用---》  http://localhost:1234/emps
    @Autowired
    private UserService userService;

    /**
     *
     * //他的，@GetMapping是一个组合注解，是@RequestMapping(method = RequestMethod.GET)的缩写。
     * @param model
     * @return
     * http://localhost:1234/emps
     */

    @GetMapping("/emps")
    public String getEmps(Model model){
        Collection<UserEntity> entities =userService.findall();
        //放入域中
          model.addAttribute("entities",entities);
        System.out.println("entities//////////=="+entities);
        // thymeleaf默认就会拼串
        // classpath:/templates/xxxx.html
        return "index";
      //  return new ModelAndView("index");
    }

    //删除员工
    @RequestMapping (value = "/delect/{id}",method = RequestMethod.POST)
    public String deleteEmp(@PathVariable("id") Integer id) {
        //根据id删除员工
        userService.delectId(id);
        //删除后返回本页面 主页
        return "redirect:/emps";
    }


/**
 *
 */
    //回显页面
    @RequestMapping (value = "/selectUp/{id}")
    public String selectUp(@PathVariable("id") Integer id,Model model) {
        UserEntity userEntity = userService.getselectId(id);
        System.out.println("进来了update"+userEntity);
        model.addAttribute("userEntity",userEntity);

        //删除后返回本页面 主页
        return "update";
    }





    /**
     *  修改
     * @param userEntity
     * @return
     *
     * http://localhost:1234/user/updatess
     */
    @RequestMapping("/updatehtml")
    public String Upate(UserEntity userEntity){
        int results=userService.update(userEntity);

        if(results==1){
            System.out.println("更新成功");
        }else {
            System.out.println("更新失败");
        }

        return "index";
    }




}
