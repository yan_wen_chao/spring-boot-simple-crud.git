package com.example.demo.mapper;


import com.example.demo.entity.UserEntity;

import java.util.List;

public interface UserinfoMapper {
    /**
     * 查询全部
     * @return
     */
    List<UserEntity> findall();

    /**
     * 根据id查询
     * @param id
     * @return
     */
    UserEntity getselectId(int id);

    /**
     * 根据id删除
     * @param id
     * @return
     */
    int delectId(int id);


    /**
     * 新增数据
     * @param userEntity
     * @return
     */
    int addall(UserEntity userEntity);

    /**
     * 修改数据id
     * @param userEntity
     * @return
     */
    int update(UserEntity userEntity);
}
