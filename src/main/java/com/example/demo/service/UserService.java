package com.example.demo.service;

import com.example.demo.entity.UserEntity;

import java.util.List;

public interface UserService {
    /**
     * 查询所有的数据
     * @return
     */
    List<UserEntity> findall();

    /**
     * 根据iD查询数据
     * @param id
     * @return
     */
    UserEntity getselectId(int id);

    /**
     * 根据id删除数据
     * @param id
     * @return
     */
    int delectId(int id);

    /**
     * 新增数据
     * @param userEntity
     * @return
     */
    int addall(UserEntity userEntity);

    /**
     * 修改数据
     * @param userEntity
     * @return
     */
    int update(UserEntity userEntity);
}
