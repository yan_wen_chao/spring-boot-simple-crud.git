package com.example.demo.service.impl;

import com.example.demo.entity.UserEntity;
import com.example.demo.mapper.UserinfoMapper;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service  //。@Service用于标注业务层组件
public class UserinfoServiceImpl implements UserService {
  //引入dao接口
    @Autowired
    private UserinfoMapper userinfoMapper;

    /**
     * 调用finall 实现查询全部
     * @return
     */
    @Override
    public List<UserEntity> findall() {
        return userinfoMapper.findall();
    }

    /**
     * 调用getselectId 实现根据id查询
     * @param id
     * @return
     */
    @Override
    public UserEntity getselectId(int id) {
        return userinfoMapper.getselectId(id);
    }

    /**
     * 根据id 删除对应的数据
     * @param id
     * @return
     */
    @Override
    public int delectId(int id) {
        return userinfoMapper.delectId(id);
    }

    /**
     * 新增数据
     * @param userEntity
     * @return
     */
    @Override
    public int addall(UserEntity userEntity) {
        return userinfoMapper.addall(userEntity);
    }

    /**
     * 修改数据 根据id
     * @param userEntity
     * @return
     */
    @Override
    public int update(UserEntity userEntity) {
        return userinfoMapper.update(userEntity);
    }
}
